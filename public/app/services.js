/**
 * Yelp API
 */
app.factory("Yelp", [
    '$resource',
    '$q',
    'ENV_VARS',

    function($resource, $q, ENV_VARS ) {

        return function() {

            var factory = {

                /*
                 * ng-model attributes for a search form
                 */
                terms       : null,
                location    : null,
                limit       : ENV_VARS.SEARCH_RETURN_LIMIT,

                /*
                 * Yelp functionality
                 */
                search      : function () {

                    var deferred = $q.defer();
                    var location;

                    var api = $resource('/api/yelp/search/:id',
                        { id: '@id' },
                        { save : { method: "POST", isArray: true } }
                    );

                    if( angular.equals( factory.location, null ) ) {
                        deferred.reject( { data: { msg : 'Please enter a location (e.g. zipcode, city, etc)' } } );
                    }
                    else {

                        api.save( factory , function ( response ) {

                            deferred.resolve( response );

                        }, function( error ) {

                            deferred.reject( error );
                        });
                    }

                    return deferred.promise;
                },

                business    : function ( id ) {
                    var deferred = $q.defer();

                    var api = $resource('/api/yelp/:id',
                        { id: '@id' },
                        { get: { method: "GET", isArray: false } }
                    );

                    api.get( { id: id }, function ( response ) {
                        deferred.resolve( response );
                    });

                    return deferred.promise;
                },

                setLocation : function( location ) {
                    factory.location = location;
                }


            };

            return factory;
        }
    }
]);

/**
 * User-related data
 */
app.factory( "User", [
    '$resource',
    '$q',
    '$sanitize',
    'Slug',
    'Profile',

    function($resource, $q, $sanitize, Slug, Profile) {

        return function () {

            var new_factory = {};

            var default_factory = {
                id          : null,
                email       : null,
                first_name  : null,
                last_name   : null,
                credentials : {
                    email               : null,
                    password            : null,
                    password_confirm    : null
                },
                logged_in   : false,
                profile     : Profile(),

                register        : function ( profile ) {
                    var api = $resource( '/api/profile/register' );

                    var payload = profile;

                    angular.extend( payload, new_factory.credentials );

                    var deferred = $q.defer();


                    Profile().slugExists( new_factory.profile.temp_slug ).then( function() {

                        // Slug doesn't exist. Let's save the user
                        api.save( payload, function ( response ) {

                            new_factory.setUser( response, false );

                            // Set the user object properties
                            new_factory.credentials.email = $sanitize( new_factory.credentials.email );
                            new_factory.credentials.password = $sanitize( new_factory.credentials.password );

                            deferred.resolve( response );
                        },
                        function( error ) {
                            deferred.reject( error );
                        });

                    }, function( error ) {

                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                login           : function () {
                    var api = $resource('/api/user/login/');

                    var deferred = $q.defer();

                    api.save( new_factory.credentials, function ( response ) {

                        new_factory.setUser( response );

                        new_factory.logged_in = true;

                        new_factory.clearCredentials();

                        deferred.resolve( response );

                    }, function ( error ) {
                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                setUser         : function( user, clear_password ) {

                    if( angular.equals( null, clear_password ) ) {
                        clear_password = true;
                    }

                    var temp = {};

                    angular.copy( this.profile, temp );

                    angular.extend( this, user );

                    temp.set( user.profile );

                    this.profile = temp;

                    if( true === clear_password ) {
                        this.credentials.password = this.credentials.password_confirm = null;
                    }

                },

                clearCredentials : function() {
                    new_factory.credentials.password = new_factory.credentials.password_confirm = null;
                },

                pwForget        : function () {
                    var api = $resource('/api/user/password-forget');

                    var deferred = $q.defer();

                    api.save( { email: new_factory.credentials.email }, function ( response ) {

                        deferred.resolve( response );

                    }, function (error) {
                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                pwReset         : function ( id, code ) {
                    var api = $resource('/api/user/password-reset');

                    var deferred = $q.defer();

                    // Use the credentials object as the payload
                    new_factory.credentials.reset_code = code;

                    new_factory.credentials.id = id;

                    api.save( new_factory.credentials, function ( response ) {

                        deferred.resolve( response );

                    }, function (error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },

                logout          : function () {
                    var api = $resource('/api/user/logout');

                    var deferred = $q.defer();

                    api.get( function (response) {

                        // Reset the user object
                        new_factory.logged_in = false;

                        new_factory.clearCredentials();

                        deferred.resolve(response);
                    });

                    return deferred.promise;
                },

                update          : function ( profile ) {

                    if( !angular.equals( null, new_factory.credentials.password_confirm ) ) {

                        new_factory.password_update = new_factory.credentials.password_confirm;

                        new_factory.clearCredentials();
                    }

                    var api = $resource( '/api/profile/update' );

                    var deferred = $q.defer();

                    api.save( new_factory, function ( response ) {

                        new_factory.setUser( response );

                        // See if we need to update the reminder time
                        new_factory.profile.checkNotify();

                        deferred.resolve( response );

                    }, function( error ) {

                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                isAuthenticated : function () {

                    var api = $resource('/api/user/is-authenticated/:id',
                        { id: '@id' },
                        { get: { method: "GET", isArray: true } }
                    );

                    var deferred = $q.defer();

                    api.get( function ( response ) {

                        // User is authenticated
                        var user = response[0];

                        delete user.permissions;

                        new_factory.setUser( user );

                        new_factory.logged_in = true;

                        deferred.resolve( user );
                    },

                    function (error_object) {

                        // If using a 401 interceptor, this may never execute
                        deferred.reject(error_object);
                    });

                    return deferred.promise;
                },
            };

            angular.extend( new_factory, default_factory );

            return new_factory;
        }
    }
]);

/**
 * User's Profile data
 */
app.factory( "Profile", [
    '$resource',
    '$q',
    'RandomString',
    'ENV_VARS',
    '$rootScope',
    'Slug',

    function($resource, $q, RANDSTR, ENV_VARS, $rootScope, Slug ) {

        return function() {

            var new_factory = {};

            var default_factory = {
                id                  : 1,            // Default, guest user
                display_name        : null,
                slug                : null,
                phone               : null,
                address             : null,
                city                : null,
                state               : null,
                zipcode             : null,
                unique              : null,
                webhook_url_in      : null,
                webhook_token       : null,
                reminder_time       : new Date( ( new Date().getTime() + ( 120 * 60000 ) ) ),  // Two hours ahead
                utc_offset          : new Date().getTimezoneOffset(),
                group_email         : null,
                temp_name           : null,
                temp_slug           : RANDSTR(),
                is_guest            : true,
                notify_enable       : false,
                slug_retry          : false,        // Used in the app only
                new_profile         : true,         // Used in the app only
                show_winner         : false,        // Used in the app only
                reminder_updated    : 0,            // Used in the app only

                getProfile          : function( full_profile ) {

                    var get_votes = full_profile ? full_profile : false;

                    var api = $resource( '/api/profile/show' );

                    var deferred = $q.defer();

                    // Get the profile, with votes is optional and with an overall search-results limit
                    api.save( { slug: this.slug, votes: get_votes, limit: ENV_VARS.SEARCH_RETURN_LIMIT }, function ( response ) {

                        new_factory.set( response );

                        deferred.resolve( response );

                    }, function( error ) {

                        // Parse the error. Could be a guest profile problem

                    });

                    return deferred.promise;
                },

                getZip              : function() {
                    return this.zipcode;
                },

                set                 : function( profile ) {

                    angular.extend( this, profile );

                    // Set the profile's reminder time object
                    var time = profile.reminder_time.date ? profile.reminder_time.date : profile.reminder_time;

                    this.reminder_time = new Date( time );

                    if( this.notify_enable === 0 || this.notify_enable === false ) {
                        this.notify_enable = false;
                    }
                    else {
                        this.notify_enable = true;
                    }

                    // Clear this flag
                    this.new_profile = false;

                    this.resetTemp();

                },

                resetTemp           : function() {

                    this.temp_name = this.display_name;

                    this.temp_slug = this.slug;
                },

                slugExists          : function( slug ) {
                    var api = $resource('/api/profile/exists/:slug',
                        { slug: '@slug' }
                    );

                    var deferred = $q.defer();

                    api.get( { slug: slug }, function ( response ) {

                        // Slug doesn't exist
                        new_factory.slug = slug;

                        // Clear the retry property
                        new_factory.slug_retry = false;

                        deferred.resolve( response );

                    }, function ( error ) {

                        // Slug exists. Should we retry?
                        if( new_factory.slug_retry ) {

                            new_factory.slug = RANDSTR();

                            new_factory.slugExists( new_factory.slug );
                        }

                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                updateNotify        : function() {
                    var api = $resource('/api/profile/update-notification/:profile_id',
                        { profile_id: '@profile_id' }
                    );

                    var deferred = $q.defer();

                    api.get( { profile_id: this.id }, function ( response ) {

                        new_factory.reminder_updated = 0;

                        // Time updated
                        deferred.resolve( response );

                    }, function ( error ) {

                        // Time not updated
                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                createGuest         : function( postal_code ) {

                    var api = $resource( '/api/profile/create-guest' );

                    var deferred = $q.defer();

                    // Set a temporary slug to try and create a profile with
                    new_factory.slug = new_factory.temp_slug;

                    new_factory.display_name = new_factory.slug;

                    new_factory.zipcode = postal_code;

                    api.save( new_factory, function( response ) {
                        new_factory.set( response );

                        deferred.resolve( new_factory );
                    });

                    return deferred.promise;
z
                },

                checkNotify         : function() {
                    if( this.reminder_updated > 0 ) {
                        this.updateNotify();
                    }
                },

            };

            $rootScope.$watch(function() {
                return new_factory.temp_name;

            }, function tempNameChanged(newValue, oldValue) {

                if( !angular.equals( null, newValue ) && newValue.length > 0 ) {
                    new_factory.temp_slug = Slug.slugify( newValue );
                }
            });

            angular.copy( default_factory, new_factory );

            return new_factory;
        }
    }
]);

/**
 * Profile's voting pool and vote related data Factory
 */
app.factory( "Votes", [
    '$resource',
    '$q',
    'Yelp',
    'ENV_VARS',

    function( $resource, $q, Yelp, ENV_VARS ) {

        return function () {

            var factory = {

                voting_pool     : [],       // All available places to choose from
                selected        : 0,        // Chosen places
                the_payload     : {},       // Yelp ID, vote and vote comment for each vote
                places          : [],
                slug            : null,
                voted           : false,

                updatePool  : function( places, slug ) {

                    factory.voting_pool = [];

                    factory.places = places;

                    factory.slug = slug;

                    // Add any existing votes
                    if( 0 < factory.selected ) {
                        angular.forEach( factory.the_payload, function( item, index ) {
                            factory.voting_pool.push( item );
                        });
                    }

                    // Add new items returned from a search
                    angular.forEach( places, function( item, index ) {
                        if( ENV_VARS.SEARCH_DISPLAY_LIMIT > factory.voting_pool.length ) {

                            // Need to initialize these here for the view
                            item.selected = null;
                            item.hide_detail = true;

                            factory.voting_pool.push( item );
                        }
                    });
                },

                select      : function ( new_place, value ){
                    var duplicate = false;
                    var item = { vote: value, new_comment: null };

                    // Don't add duplicate votes, but change the value if needed
                    angular.forEach( factory.the_payload, function( place, id ) {

                        if( id === new_place.id ) {

                            factory.the_payload[ id ][ 'vote' ] = value;

                            new_place.selected = value;

                            duplicate = true;
                        }
                    });


                    //if ( false === duplicate && ENV_VARS.VOTE_LIMIT > factory.selected ) {
                    if ( false === duplicate && ENV_VARS.VOTE_LIMIT > factory.selected ) {

                        factory.selected++;

                        new_place.selected = value;

                        angular.extend( new_place, item );

                        factory.the_payload[ new_place.id ] = new_place;
                    }
                },

                vote        : function ( profile ) {

                    var deferred = $q.defer();

                    var api = $resource('/api/vote/:id',
                        {id: '@id'},
                        { save: { method: "POST", isArray: false } }
                    );

                    api.save( { profile: profile, votes: factory.the_payload }, function ( response ) {

                        factory.voted = true;

                        deferred.resolve( response );

                    }, function ( error ) {

                        deferred.reject( error );
                    });

                    return deferred.promise;
                },

                clear       : function() {
                    factory.voting_pool = [];
                },

                defaultZip  : function() {
                    var zip;

                    angular.forEach( factory.the_payload, function( item, index ) {

                        if( item.vote === true ) {
                            zip = item.zipcode;
                        }
                        else {
                            zip = item.zipcode;
                        }
                    });

                    return zip;
                },

                remove      : function( place ) {

                    place.new_comment = null;
                    place.vote = null;

                    // Remove this item from the voting pool altogether
                    angular.forEach( factory.the_payload, function( item, index ) {
                        if( index === place.id ) {
                            delete factory.the_payload[ index ];
                            factory.selected--;
                        }
                    });
                }

            };

            return factory;
        }
    }
]);

/**
 * Returns an image URL with the large version of the image
 *
 * @param input
 * @returns {*}
 */
app.filter('big_image', function() {

    return function(input) {

        if( !angular.equals( input, null ) ) {
            var uris = input.split(".");

            var suffix = uris[ uris.length - 2 ];

            var suffix_uris = suffix.split("/");

            // Set the new image size
            // s.jpg: up to 40×40
            // ss.jpg: 40×40 square
            // m.jpg: up to 100×100
            // ms.jpg: 100×100 square
            // l.jpg: up to 600×400
            // ls.jpg: 250×250 square
            // o.jpg: up to 1000×1000
            // 348s.jpg: 348×348 square
            suffix_uris[ suffix_uris.length - 1 ] = "348s";

            suffix = suffix_uris.join("/");

            uris[ uris.length - 2 ] = suffix;

            input = uris.join(".");
        }

        else {

            // Placeholder image
            input = "../assets/no-image.png";
        }

        return input;
    }
});

app.factory('RandomString', [
    '$window',

    function (w) {

    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var Math = w.Math;

    return function randomString(length) {
        length = length || 15;
        var string = '', rnd;
        while (length > 0) {
            rnd = Math.floor(Math.random() * chars.length);
            string += chars.charAt(rnd);
            length--;
        }
        return string;
    };
}]);

app.factory('BodyClass', [


    function() {

        return function() {

            var new_factory = {
                class       : '',
                background  : '',

                set     :  function( new_class ) {
                    new_factory.class = new_class;
                },

                setBg   : function( bg ) {
                    new_factory.background = bg;
                }

            };

            return new_factory;
        };
    }
]);