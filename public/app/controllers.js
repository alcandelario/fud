/**
 * Created by alcandelario on 12/3/14.
 */

app.controller( 'main.controller', [
    '$scope',
    'authService',
    'ENV_VARS',
    '$state',
    'toaster',
    '$stateParams',
    'RandomString',
    '$cookieStore',
    '$modal',

    function( $scope, authService, ENV_VARS, $state, toaster, $stateParams, RANDSTR, $cookieStore, $modal ) {

        $scope.bodyClass.set( $state.current.name );

        $scope.edit_location = false;

        /**
         * Search the Yelp API and prep for voting
         */
        $scope.search = function() {

            $scope.yelp.search().then( function( response ) {

                $scope.votes.updatePool( response, $scope.profile.slug );

                $state.go('profile', { profile_slug : $scope.profile.slug } );

            },

            function( error ) {

                    toaster.pop('error', '', error.data.msg );
            });
        };

        $scope.logout = function() {

            $scope.user.logout().then( function( response ) {

                    toaster.pop('info', '', response.msg );

                    $cookieStore.remove( 'show_winner_panel' );

                    $state.go( 'profile', { profile_slug: $scope.profile.slug } );
            });
        };

        $scope.login = function() {

            $scope.user.login().then( function( response ) {

                    // Fire the login event for anyone who needs it
                    authService.loginConfirmed();

                    $cookieStore.remove( 'show_winner_panel' );

                    // Don't go to a new state if the login form is integrated into a directive
                    if( !ENV_VARS.LOGIN_PARTIAL ) {
                        $state.go( 'profile', { profile_slug: $scope.user.profile.slug } );
                    }

            }, function( error ) {
                    toaster.pop('error', '', error.data.msg );
                }
            );
        };

        $scope.register = function()  {

            $scope.user.register( $scope.user.profile ).then( function( response ) {

                // Show a new user modal if they register without having aready gone thru the voting process
                if( !angular.isDefined( $scope.user.new_register ) ) {

                    setTimeout( function() {
                        var modal = $modal.open({
                            templateUrl: '../views/just-registered-modal.html',
                            controller: 'guest-register-modal.controller',
                            size: 'lg'
                        });
                    }, 300 );
                }

                if( 0 < response.profile.id ) {

                    if( !ENV_VARS.EMAIL_ACTIVATION_ENABLED ) {

                        $scope.profile.set( $scope.user.profile );

                        $scope.user.login().then( function() {

                            toaster.pop('success', '', 'You are now logged in.' );

                            $scope.user.profile.checkNotify();

                            $state.go( 'profile', { profile_slug: $scope.user.profile.slug } );

                        }, function( error ) {
                            toaster.pop('warning', '', 'Keep using the site, but we had problems logging you in. Try again later.' );
                        });
                    }
                }
            },

            function( error ) {
                toaster.pop('error', '', error.data.msg );
                $state.go('profile-register');
            });
        };

        $scope.home = function() {

            if( true === $scope.user.logged_in ){

                $state.go( 'profile', { profile_slug: $scope.user.profile.slug } );

            }
            else {
                $scope.profile.slug = null;

                $state.go( 'home' );
            }
        };

        $scope.pwForget = function() {

            $scope.user.pwForget().then( function( response ) {

                    toaster.pop('success', '', response.msg );

            }, function( error ) {
                toaster.pop('error', '', error.data.msg );
            });
        };

        $scope.pwReset = function() {

            // Grab the user ID and reset token from the url
            var code = $stateParams[ 'reset_code' ];

            var id = $stateParams[ 'id' ];

            $scope.user.pwReset( id, code ).then( function( response ) {

                toaster.pop('success', '', response.msg );

                $state.go('login');

            }, function( error ) {
                toaster.pop('error', '', error.data.msg );
            });
        }

    }
]);

app.controller ( 'secured.pages.controller', [
    '$scope',
    '$rootScope',
    'authService',
    '$state',
    '$stateParams',
    'Slug',
    'ENV_VARS',
    'toaster',

    function( $scope, $rootScope, authService, $state, $stateParams, Slug, ENV_VARS, toaster ) {

        $scope.bodyClass.set( $state.current.name );

        // If user is on any route that requires authorization and
        // they aren't logged in, redirect.
        $scope.$on('event:auth-loginRequired', function() {
            $state.go('login');
        });

        // Update user profile
        $scope.updateProfile = function() {

            $scope.user.update().then( function( response ) {

                $scope.error = {};

                $scope.profile.set( $scope.user.profile );

                $scope.yelp.setLocation( $scope.user.profile.zipcode )

                toaster.pop('success', '', "Profile updated!" );

                //$scope.user.profile.checkNotify();

                // Redirect, just in case profile name/slug were updated
                $state.go( 'profile-edit', { profile_slug: $scope.user.profile.slug } );
            },

            function ( error ) {

                toaster.pop('error', '', error.data.msg );

                // Reset the form fields because updating the profile name/slug likely didn't work
                $scope.user.profile.resetTemp();
            });
        }
    }
]);

app.controller( 'home.controller', [
    '$scope',
    '$stateParams',
    '$state',
    //'$rootScope',
    'ENV_VARS',
    'toaster',

    function( $scope, $stateParams, $state, ENV_VARS, toaster ) {

        $scope.bodyClass.set( $state.current.name );

        $scope.bodyClass.setBg( 'home' );
    }
]);

app.controller( 'profile.controller', [
    '$scope',
    '$stateParams',
    '$state',
    'ENV_VARS',
    'toaster',
    '$modal',
    '$cookieStore',
    '$timeout',

    function( $scope, $stateParams, $state, ENV_VARS, toaster, $modal, $cookieStore, $timeout ) {

        var reload = false;

        $scope.bodyClass.set( $state.current.name );

        $scope.profile.slug = $stateParams[ 'profile_slug' ];

        // Reload the voting pool info if we were looking at another profile before
        if( !angular.equals( $scope.votes.slug, $scope.profile.slug ) ) {
            reload = true;
        }

        // Make sure its a legit slug first
        $scope.profile.slugExists( $scope.profile.slug ).then( function( response ) {

            // Doesn't exist. the page was likely refreshed
            if( 0 === $scope.votes.voting_pool.length ) {
                $state.go('home');
            }
        }, function( response ) {

            // Slug exists, so let's get the complete profile
            if( true === reload || 0 === $scope.votes.voting_pool.length ) {

                // "True" allows us to get profile meta data + the voting pool
                $scope.profile.getProfile( 'true' ).then( function ( response ) {

                    $scope.profile.set( response );

                    $scope.votes.clear();

                    $scope.votes.updatePool( response.voting_pool, $scope.profile.slug );

                    // Set a default zip code into the Yelp object
                    $scope.yelp.setLocation( $scope.profile.getZip() );

                    $scope.toggleWinnerPanel();
                });
            }
        });

        /* **********************
         *  Functions
         ***********************/

        /**
         * Hide the panel for "winning vote"
         * @param voted
         */
        $scope.toggleWinnerPanel = function( voted ) {

            if( angular.isDefined( voted ) ) {
                $scope.profile.show_winner = false;

                $cookieStore.put( 'show_winner_panel', $scope.profile.show_winner );
            }

            else if( angular.isDefined( $scope.profile.winner ) ) {

                $scope.profile.show_winner = !$scope.profile.show_winner;

                $cookieStore.put( 'show_winner_panel', $scope.profile.show_winner );
            }
        };

        /**
         * Selects the given place and hides the winning vote panel
         * if showing
         *
         * @param business
         * @param vote
         */
        $scope.select = function( business, vote ) {

            $scope.toggleWinnerPanel( true );

            $timeout( function() { $scope.votes.select( business, vote ) }, 200 );
        };

        /**
         * Submits the user votes and creates a guest profile if necessary
         */
        $scope.send = function() {

            // If a guest, create profile before voting
            if( true === $scope.profile.new_profile ) {

                var zip = $scope.votes.defaultZip();

                $scope.profile.createGuest( zip ).then( function( response ) {

                    // Initialize the user profile
                    $scope.user.profile.set( $scope.profile );

                    $scope.votes.vote( $scope.profile ).then( function( response ) {

                        // Prompt this new user to register this one time only
                        setTimeout( function() {
                            var modal = $modal.open({
                                templateUrl: '../views/guest-register-modal.html',
                                controller: 'guest-register-modal.controller',
                                size: 'lg'
                            });
                        }, 300 );

                        $state.go( 'profile', { profile_slug : $scope.profile.slug } );
                    });
                });
            }

            // Just send the votes
            else {
                $scope.votes.vote( $scope.profile ).then( function( response ) {
                    toaster.pop('success', '', response.msg );

                }, function( response ) {

                    toaster.pop('danger', '', response.data.msg );
                });
            }
        }

        /* **********************
         *  Watches
         * **********************/

        $scope.$watch( 'votes.selected', function() {

            if( $scope.votes.selected > 0 ) {

                $scope.hideSendBtn = false;
            }
        });

        $scope.$watch( 'profile', function( newV, oldV ) {

            var cookie = $cookieStore.get( 'show_winner_panel' );

            if( angular.isDefined( $scope.profile.winner ) ) {

                if( angular.isUndefined( cookie ) ) {
                    cookie = true;
                }

                $scope.profile.show_winner = cookie;
            }

        }, true );

    }
]);

app.controller( 'guest-register-modal.controller', [
    '$scope',
    '$stateParams',
    '$state',
    '$modalInstance',
    'toaster',
    'ENV_VARS',

    function( $scope, $stateParams, $state, $modalInstance, toaster, ENV_VARS ) {

        $scope.final_step = false;

        $scope.start_register = false;

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.finish = function() {
            $scope.final_step = true;
            $scope.user.new_register = true;
        };

        $scope.register = function()  {

            $scope.user.register( $scope.user.profile ).then( function( response ) {

                    if( 0 < response.profile.id ) {

                        if( !ENV_VARS.EMAIL_ACTIVATION_ENABLED ) {

                            $scope.profile.set( $scope.user.profile);

                            $scope.user.login().then( function() {

                                toaster.pop('success', '', 'Profile created! Logging you in.' );

                                $state.go( 'profile', { profile_slug: $scope.user.profile.slug } );

                            }, function( error ) {
                                toaster.pop('warning', '', 'Keep using the site, but we had problems logging you in. Try again later.' );
                            });
                        }
                    }
                },

                function( error ) {
                    toaster.pop('error', '', error.data.msg );
                    $state.go('profile-register');
                });
        };
    }
]);