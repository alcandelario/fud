module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    './public/css/app.css': './src/scss/screen.scss',
                }
            },
            dev: {
                options: {
                    style: 'nested'
                },
                files: {
                    './public/css/app.css': './src/scss/screen.scss',
                }
            }
        },
        jshint: {
            options: {
                browser: true,
                '-W030': true,
                globals: {
                    jQuery: true
                },
            },
            all: ['./src/js/**/*.js', 'Gruntfile.js']
        },
        watch: {
            options: {
                livereload: true,
            },
            sass: {
                files: ['**/*.scss'],
                tasks: ['sass:dist']
            },
            //scripts: {
            //    files: ['./assets/js/**/*.js', 'Gruntfile.js'],
            //    tasks: ['jshint']
            //}
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-rsync');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['scss:dist']);
    grunt.registerTask('deploy-staging', ['rsync:stage','rsync:stage_plugins','rsync:stage_uploads']);
    grunt.registerTask('deploy-production', ['rsync:prod','rsync:prod_plugins','rsync:prod_uploads']);


};