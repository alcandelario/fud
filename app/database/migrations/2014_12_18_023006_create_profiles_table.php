<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create( 'profiles' , function($table)
		{
			$table->increments( 'id' )->unsigned();
			$table->integer( 'user_id' )->unsigned();
			$table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
			$table->string( 'display_name' );
			$table->string( 'slug' )->unique();
			$table->string( 'city' )->nullabe();
			$table->string( 'state' )->nullable();
			$table->string( 'zipcode' )->nullable();
			$table->string( 'group_email' )->nullable();
			$table->string( 'webhook_url_in' )->nullable();
			$table->string( 'webhook_token' )->nullable();
			$table->boolean( 'notify_enable' )->nullable();
			$table->dateTime( 'reminder_time' )->nullable(); // Configurable num minutes to send notifications
			$table->integer( 'utc_offset' )->nullable();
			$table->boolean( 'is_guest' )->nullable();
			$table->timestamps();

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table( 'profiles' , function( Blueprint $table ) {
			$table->dropForeign( 'profiles_user_id_foreign' );
		});

		Schema::drop( 'profiles' );
	}

}
