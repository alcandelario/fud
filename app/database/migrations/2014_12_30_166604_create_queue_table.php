<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueueTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'queue', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('queue_id');
            $table->string('delete');
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'queue', function(Blueprint $table) {
            $table->dropForeign('queue_profile_id_foreign');
        });
        Schema::drop( 'queue' );
    }

}