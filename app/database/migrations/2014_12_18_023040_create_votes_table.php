<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create( 'votes', function($table)
		{
			$table->increments(	'id' )->unsigned();
			$table->integer( 'profile_id' )->unsigned();
			$table->foreign( 'profile_id' )->references( 'id' )->on( 'profiles' )->onDelete( 'cascade' );
			$table->boolean( 'vote' );
			$table->integer( 'suggest_index' )->nullable();
			$table->timestamps();

			$table->engine = 'InnoDB';
		});

		Schema::create( 'comments', function($table)
		{
			$table->increments( 'id' )->unsigned();
			$table->string( 'comment' );
			$table->timestamps();

			$table->engine = 'InnoDB';
		});

		Schema::create( 'places', function(Blueprint $table)
		{
			$table->increments( 'id')->unsigned();
			$table->string( 'yelp_id' );
			$table->string( 'url' )->nullable();
			$table->string( 'rating' )->nullable();
			$table->string( 'name' )->nullable();
			$table->string( 'display_phone' )->nullable();
			$table->string( 'phone' )->nullable();
			$table->string( 'image_url' )->nullable();
			$table->string( 'cross_streets' )->nullable();
			$table->string( 'address' )->nullable();
			$table->string( 'zipcode' )->nullable();
			$table->timestamps();

			$table->engine = 'InnoDB';
		});


	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table( 'votes' , function(Blueprint $table) {
			$table->dropForeign( 'votes_profile_id_foreign' );
		});

		Schema::drop( 'votes' );
		Schema::drop( 'comments' );
		Schema::drop( 'places' );

	}

}
