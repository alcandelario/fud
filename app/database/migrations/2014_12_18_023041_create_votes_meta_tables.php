<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVotesMetaTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('votes_places', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('vote_id')->unsigned();
			$table->foreign('vote_id')->references('id')->on('votes')->onDelete('cascade');
			$table->integer('place_id')->unsigned();
			$table->foreign('place_id')->references('id')->on('places');
			$table->timestamps();

			$table->engine = 'InnoDB';
		});

		Schema::create('votes_comments', function(Blueprint $table)
		{
			$table->increments('id')->unsigned();
			$table->integer('vote_id')->unsigned();
			$table->foreign('vote_id')->references('id')->on('votes')->onDelete('cascade');
			$table->integer('comment_id')->unsigned()->onDelete('cascade');
			$table->foreign('comment_id')->references('id')->on('comments');
			$table->timestamps();

			$table->engine = 'InnoDB';
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('votes_places', function(Blueprint $table) {
			$table->dropForeign('votes_places_vote_id_foreign');
			$table->dropForeign('votes_places_place_id_foreign');
		});

		Schema::table('votes_comments', function(Blueprint $table) {
			$table->dropForeign('votes_comments_vote_id_foreign');
			$table->dropForeign('votes_comments_comment_id_foreign');
		});

		Schema::drop('votes_places');
		Schema::drop('votes_comments');

	}

}
