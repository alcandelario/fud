<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesResultsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		//
		Schema::create('votes_results', function($table)
		{
			$table->increments( 'id' )->unsigned();
			$table->string( 'yelp_id' );
			$table->string( 'category' );
			$table->integer( 'profile_id' )->unsigned();
			$table->foreign( 'profile_id' )->references( 'id' )->on( 'profiles' )->onDelete( 'cascade' );
			$table->timestamps();

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table( 'votes_results', function(Blueprint $table) {
			$table->dropForeign( 'votes_results_profile_id_foreign' );
		});

		Schema::drop( 'votes_results' );
	}
}
