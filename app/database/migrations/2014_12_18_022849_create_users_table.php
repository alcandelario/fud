<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{


		/* *************************************************
		 *
		 *  If not using Sentry for user management,
		 *  uncomment the Schema::create lines.
		 *
		 *
		 *
		 *  If using Sentry, execute on the CLI:
		 *
		 *  php artisan migrate --package=cartalyst/sentry
		 *
		 * *************************************************/


//		Schema::create('users', function($table)
//		{
//			$table->increments('id')->unsigned();
//			$table->string('email');
//			$table->string('password');
//			$table->string('first_name')->nullable();
//			$table->string('last_name')->nullable();
//			$table->boolean('confirmed')->default(false);
//			$table->string('remember_token', 100);
//			$table->string('id_code')->default(0);
//			$table->string('pw_code')->default(0);
//			$table->timestamps();
//		});
//
//		// Creates password reminders table
//		Schema::create('password_reminders', function($t)
//		{
//			$t->string('email');
//			$t->string('token');
//			$t->timestamp('created_at');
//		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

//		Schema::drop('password_reminders');
		//Schema::drop('users');

	}

}
