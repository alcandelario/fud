<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlacklistTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('blacklists', function($table)
		{
			$table->increments('id')->unsigned();
			$table->string('yelp_id');
			$table->integer('profile_id')->unsigned();
			$table->foreign('profile_id')->references('id')->on('profiles')->onDelete('cascade');
			$table->boolean('permanent')->nullable();
			$table->timestamps();

			$table->engine = 'InnoDB';
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('blacklists', function(Blueprint $table) {
			$table->dropForeign('blacklists_profile_id_foreign');
		});

		Schema::drop('blacklists');
	}

}
