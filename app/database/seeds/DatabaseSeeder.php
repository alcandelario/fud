<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
	}

}


class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		Sentry::createUser(
			array(
				'email' 		=> 'guest@guest.com',
				'password' 		=> '74rR88T837ITCcd',
				'activated'		=> 0
			)
		);

		Sentry::createUser(
			array(
				'email' 		=> 'al@doejo.com',
				'password' 		=> 'password',
				'activated'		=> 1
			)
		);

		Sentry::createUser(
			array(
				'email' 		=> 'brevega@gmail.com',
				'password' 		=> 'password',
				'activated'		=> 1
			)
		);

		Sentry::createUser(
			array(
				'email' 		=> 'wccandelario@gmail.com',
				'password' 		=> 'password',
				'activated'		=> 1
			)
		);

		Profile::create(
			array(
				'user_id'			=> Sentry::findUserByLogin('al@doejo.com')->id,
				'display_name'		=> "Doejo",
				'slug'				=> "doejo",
				'zipcode'			=> "60657",
				'group_email'		=> "al@doejo.com",
				'webhook_url_in'	=> "https://hooks.slack.com/services/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",
				'notify_enable'		=> 1,
				'reminder_time'		=> Carbon::createFromTime( 17, 45, 00, 'Atlantic/Reykjavik' ), 		// 15 mins before noon
				'utc_offset'		=> Config::get( 'constants.DEFAULT_UTC_OFFSET' ),
				'is_guest'			=> 0
			)
		);

		Profile::create(
			array(
				'user_id'			=> Sentry::findUserByLogin('brevega@gmail.com')->id,
				'display_name'		=> "West Suburban Nights",
				'slug'				=> "west-suburban-nights",
				'zipcode'			=> "60302",
				'reminder_time'		=> Carbon::createFromTime( 05, 45, 00, 'Atlantic/Reykjavik' ), 		// 15 mins before midnight
				'utc_offset'		=> Config::get( 'constants.DEFAULT_UTC_OFFSET' ),
				'is_guest'			=> 0
			)
		);

		Profile::create(
			array(
				'user_id'			=> Sentry::findUserByLogin('wccandelario@gmail.com')->id,
				'display_name'		=> "Bill",
				'slug'				=> "bill",
				'zipcode'			=> "10033",
				'reminder_time'		=> Carbon::createFromTime( 17, 45, 00, 'Atlantic/Reykjavik' ), 		// 12:45spm lunchtime
				'utc_offset'		=> 300,
				'is_guest'			=> 0
			)
		);
	}
}