<?php

class VotesController extends \BaseController {


	/**
	 * Store a newly created resource in storage.
	 * POST /votes
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();

		$id = $input[ 'profile' ][ 'id' ];

		$notify = (bool) Profile::checkNotify( $id );

		// Save votes and any related meta data
		foreach( $input[ 'votes' ] as $key => $vote ) {
			$vote = Vote::saveVote( $vote, $id );
		}

		// Send a response and set up any notifications
		// for this user if necessary
		if( isset( $vote[ 'saved' ] ) ) {

			if( false === $notify ) {

				$profile = Profile::find( $id );

				Profile::setInitialReminders( $profile );
			}

			return Response::json( [ 'msg' => 'Your choices are saved, thanks!' , 'profile_id' => $id, 'number_votes' => count( $input[ 'votes' ] ) ], 202 );
		}
		else {
			return Response::json( [ 'msg' => 'Looks like a problem saving votes, try again.' ], 403 );
		}

	}

	public function storeSlack() {

		$input = Input::all();

		$token = $input[ 'token' ];

		$votes = $input[ 'text' ];

		$key = $input[ 'trigger_word' ];

		$votes = str_replace( $key, '', $votes );

		$votes = explode( ',', $votes );

		$profile = Profile::select( 'id' )->where( 'webhook_token', '=', $token )->first();

		$notify = (bool) Profile::checkNotify( $profile->id );

		foreach( $votes as $suggest_index ) {

			// Get the place ID and create new votes
			$place_id = DB::table( 'votes' )
							->select( 'votes_places.place_id' )
							->join( 'votes_places', 'votes_places.vote_id', '=', 'votes.id' )
							->where( 'votes.profile_id', '=', $profile->id )
							->where( 'votes.suggest_index', '=', $suggest_index )
							->get();

			if( isset( $place_id ) ) {
				$place_id = $place_id[0]->place_id;

				// Duplicate the place
				$new_place = Place::find( $place_id )->replicate();

				$new_place->save();

				// Save a new vote
				$vote = Vote::create( array( 'profile_id' => $profile->id, 'vote' => 1 ) );

				// Save the pivot table entry as well
				DB::table( 'votes_places' )->insert( array( 'vote_id' => $vote->id, 'place_id' => $new_place->id ) );
			}
		}

		// Send a response and set up any notifications
		// for this user if necessary
		if( isset( $vote ) && false === $notify ) {

			Profile::setInitialReminders( $profile, true );

			//return Response::json( [ 'msg' => 'Your choices are saved, thanks!' , 'profile_id' => $profile_id, 'token' => $token ], 202 );
		}
		else {
		//	return Response::json( [ 'msg' => 'Looks like a problem saving votes, try again.' ], 403 );
		}

	}
}