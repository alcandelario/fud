<?php

class ProfilesController extends \BaseController {

	/**
	 * Check to make sure the slug isn't already taken
	 *
	 * @param $slug
	 * @param null $id
	 * @return bool
	 */
	public function slugExists( $slug, $id = null ) {

		$profile = DB::table('profiles')->where( 'slug', '=', $slug );

		// This method called from another internal method
		if( !empty( $id ) ) {

			$profile->where( 'user_id', '!=', $id );

			$profile = $profile->first();

			if( isset( $profile->id ) ) {

				return true;
			}

			return false;
		}

		// This method called from an API route
		else {

			$profile = $profile->first();

			$is_guest = false;

			if( isset( $profile->id ) ) {

				if( $profile->is_guest === 1 ) {

					$is_guest = true;
				}

				return Response::json( ['msg' => 'Profile name is not available', 'guest' => $is_guest ], 403 );
			}

			return Response::json( ['msg' => 'Profile name does not exist' ], 202 );
		}
	}

	public function createGuest() {

		$input = Input::all();

		return Response::json( Profile::createGuest( $input ) );
	}

	/**
	 * Register a new user and create their profile at the same time
	 *
	 * @return mixed
	 */
	public function register() {

		$input = Input::all();

		// A flag we could use to convert a guest profile into a regular one
		$temp_slug = isset( $input[ 'slug'] ) ? $input[ 'slug' ] : false;

		$guest = $input[ 'is_guest' ];

		$guest = $guest === 1 ? true : false;

		// Set reminder time
		$t = new Carbon( Config::get( 'constants.DEFAULT_REMINDER_TIME' ), Config::get( 'constants.DEFAULT_TIMEZONE' ) );

		if( isset( $input[ 'reminder_time' ] ) ) {
			$time = $input[ 'reminder_time' ];
		}

		// Convert to UTC time
		else {
			$time = $t;
			$time = $time->addMinutes( $input[ 'utc_offset' ] );
		}

		// If a guest is converting to a regular user
		if( false !== $guest ) {

			// Get the profile id so we can convert them
			$guest_id = DB::table( 'profiles' )->where( 'slug', '=', $temp_slug )->first()->id;
		}

		try {

			$user = Sentry::createUser(array(
				'email' 	=> $input['email'],
				'password' 	=> $input['password'],
				'activated' => true,
			));

			$profile_data = array(
				'user_id' 		=> $user->id,
				'display_name' 	=> $input[ 'temp_name' ],
				'slug' 			=> $input[ 'temp_slug' ],
				'zipcode' 		=> $input[ 'zipcode' ],
				'reminder_time'	=> $time,
				'group_email'	=> $input[ 'email' ], 	// Set a default reminder email
				'utc_offset' 	=> $input[ 'utc_offset' ],
				'is_guest'		=> 0,
				'notify_enable' => 1
			);

			// Either this user has never used the site, or they want to convert a guest account
			if ($user->id && ( false === $temp_slug  ) ) {

				// Create the profile
				$profile = Profile::create( $profile_data );
			}
			elseif( false !== $temp_slug ) {

				// Update the profile
				DB::table( 'profiles' )
					->where( 'id', $guest_id )
					->update( $profile_data );

				$profile = Profile::find( $guest_id );

			}

			Profile::incrementReminderDate( $profile );

		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {

			return Response::json(['msg' => 'User with this email already exists'], 403);
		}


		if( isset( $profile->id ) ) {

			// Send back the user & profile data

			$profile = Profile::localizeTime( $profile );

			$user->profile = $profile;

			return Response::json( $user );
		}
		else {

			return Response::json( [ 'msg' => 'Problem creating profile' ], 403 );
		}
	}

	/**
	 * Send basic profile info along with current
	 * voting situation if option is set
	 *
	 * @return mixed
	 */
	public function show()
	{

		$input = Input::all();

		$slug = $input[ 'slug' ];

		$votes = $input[ 'votes' ];

		$limit = $input[ 'limit' ] ? $input[ 'limit' ] : null;

		$profile = new Profile;

//		if( !$guest ) {

			$data = $profile->showProfile( $slug, $votes, $limit );

			return Response::json( $data );
//		}
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /profiles/{id}
	 *s
	 * @return Response
	 */
	public function update()
	{
		$input = Input::all();

		$slug = Profile::slugify( $input[ 'profile' ][ 'temp_name' ] );

		$profile = new Profile;

		if( !$this->slugExists( $slug, $input[ 'id' ] ) ) {

			$user = Sentry::findUserById( $input[ 'id' ] );

			$profile = $profile->find( $input[ 'profile' ][ 'id' ] );

			$user->first_name = $input[ 'first_name' ];

			$user->last_name = $input[ 'last_name' ];

			if( isset( $input[ 'password_update' ] ) ) {
				$user->password = $input[ 'password_update' ];
			}

			$profile->display_name = $input[ 'profile' ][ 'temp_name' ];
			$profile->slug = $slug;
			$profile->city = $input[ 'profile' ][ 'city' ];
			$profile->state = $input[ 'profile' ][ 'state' ];
			$profile->zipcode = $input[ 'profile' ][ 'zipcode' ];
			$profile->webhook_url_in = $input[ 'profile' ][ 'webhook_url_in' ];
			$profile->webhook_token = $input[ 'profile' ][ 'webhook_token' ];
			$profile->utc_offset = $input[ 'profile' ][ 'utc_offset' ];
			$profile->group_email = $input[ 'profile' ][ 'group_email' ];
			$profile->notify_enable = $input[ 'profile' ][ 'notify_enable' ];

			// Convert reminder time to UTC
			$t = new Carbon( $input[ 'profile' ][ 'reminder_time' ] );

			// Proper handling of reminder date-time, takes into account Date boundary
			$t = Profile::getReminderTime( $t );

			$profile->reminder_time = $t;

			$user->save();

			$profile->save();

			Profile::incrementReminderDate( $profile );

			$profile = Profile::localizeTime($profile);

			$user->profile = $profile;

			return Response::json( $user );
		}
		else {

			return Response::json( [ 'msg' => 'Sorry, "' . $input[ 'profile' ][ 'temp_name' ] . '" is taken.' ], 403 );
		}
	}

	/**
	 * If the user has votes currently, update the notification queue accordingly
	 *
	 * @param $id
	 */
	public function updateNotify( $id ) {

		$id = (int) $id;

		Profile::deleteQueuedJobs( $id );

		$profile = Profile::find( $id );

		$enable = $profile->notify_enable;

		if( true === Profile::checkNotify( $id ) && true === (bool) $profile->notify_enable ) {

			// Put new items onto the queue (reminders, emails, etc)
			// but skip the first email message
			Profile::setInitialReminders( $profile, true );

			Profile::setDailyReminder( $profile );
		}
		elseif( true === (bool) $profile->notify_enable ) {

			// Only update the queue for daily reminder
			Profile::setDailyReminder( $profile );
		}

		return Response::json( array( 'profile' => $enable ) );
	}

	public function setFavorite( $profile, $id ) {

	}

	public function clearFavorite( $profile, $id ) {

	}

	public function setBlacklist( $profile, $id ) {

	}

	public function clearBlacklist( $profile, $id ) {

	}

}