<?php

class APIController extends \BaseController {

    /**
     * Require auth for these controller routes
     */
    public function __construct()
    {
        $this->beforeFilter('serviceAuth');
    }

}