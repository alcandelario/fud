<?php

class UsersController extends \BaseController {

	public function login() {
		$input = Input::all();

		try
		{
			// Login credentials
			$credentials = array(
				'email'    => $input['email'],
				'password' => $input['password'],
			);

			// Authenticate the user
			$user = Sentry::authenticate( $credentials, true );

			Sentry::login($user, true);

			try
			{
				// Get the current active/logged in user
				$user = Sentry::getUser();

				$profile = Profile::where( 'user_id', '=', $user->id )->first();

				$profile = Profile::localizeTime( $profile );

				$user->profile = $profile;

				return Response::json( $user , 202 );


			}
			catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
			{
				// User wasn't found, should only happen if the user was deleted
				// when they were already logged in or had a "remember me" cookie set
				// and they were deleted.

				return Response::json( [ 'msg' => 'A user with that email was not found.' ], 403 );
			}

		}
		catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
		{
			return Response::json( [ 'msg' => 'Email field is required.' ], 403 );
		}
		catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
		{
			return Response::json( [ 'msg' => 'Password field is required' ], 403 );
		}
		catch (Cartalyst\Sentry\Users\WrongPasswordException $e)
		{
			return Response::json( [ 'msg' => 'Wrong password, try again.' ], 403 );
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Response::json( [ 'msg' => 'A user with that email was not found.' ], 403 );
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Response::json( [ 'msg' =>'User is not activated.' ], 403 );
		}

		// The following is only required if the throttling is enabled
		catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e)
		{
			return Response::json( [ 'msg' => 'User is suspended.' ], 403 );
		}
		catch (Cartalyst\Sentry\Throttling\UserBannedException $e)
		{
			return Response::json( [ 'msg' => 'User is banned.' ], 403 );
		}
	}

	public function logout() {

		Sentry::logout();

		if ( ! Sentry::check() )
		{
			// User is not logged in, or is not activated
			return Response::json( [ 'msg' => 'Logged out' ], 200 );
		}


	}

	public function isAuthenticated() {

		if( Sentry::check() ){

			// Get the current active/logged in user
			$user = Sentry::getUser();

			$profile = Profile::where( 'user_id', '=', $user->id )->first();

			$profile = $profile->localizeTime( $profile );

			$user->profile = $profile;

			return Response::json( [ $user ], 202);
		}

		else {
			return Response::json( [ 'msg' => 'User is not logged in.' ], 401 );
		}
	}

	public function passwordForget() {
		$input = Input::all();

		$email = $input[ 'email' ];

		$params = array();

		$params[ 'name' ] = Config::get( 'constants.EMAIL_NAME' );
		$params[ 'subject' ] = Config::get( 'constants.EMAIL_RESET' );
		$params[ 'from' ] = Config::get( 'constants.EMAIL_FROM' );

		try
		{
			// Find the user using the user email address
			$user = Sentry::findUserByLogin( $email );

			// Get the password reset code
			$resetCode = $user->getResetPasswordCode();

			$data = array('user' => $user, 'reset' => $resetCode );

			Mail::send('emails.password-reset', $data, function( $message ) use ( $user, $params )
			{

				$message->subject( $params['subject'] );

				$message->from( $params[ 'from' ], $params[ 'name' ] );

				$message->to( $user->email );

			});

			if( isset( $user->email ) ) {
				return Response::json( [ 'msg' => 'An email to reset your password is on the way!' ], 202 );
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Response::json( [ 'msg' => 'A user with that email was not found.' ], 403 );
		}
	}

	public function passwordReset() {

		$input = Input::all();

		try
		{
			// Find the user using the user id
			$user = Sentry::findUserById( $input['id'] );

			// Check if the reset password code is valid
			if ($user->checkResetPasswordCode( $input[ 'reset_code' ] ) )
			{
				// Attempt to reset the user password
				if ($user->attemptResetPassword( $input[ 'reset_code' ], $input[ 'password' ] ) )
				{
					// Password reset passed
					return Response::json( [ 'msg' => 'Password reset successfully!' ], 202 );
				}
				else
				{
					// Password reset failed
					return Response::json([ 'msg' => 'Password could not be reset.' ], 403 );
				}
			}
			else
			{
				// The provided password reset code is Invalid
				return Response::json([ 'msg' => 'The password reset code is invalid.' ], 403 );
			}
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Response::json( [ 'msg' => 'A user with that email was not found.' ], 403 );
		}
	}
}