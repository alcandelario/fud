<?php

class YelpController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /yelps
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display the specified resource.
	 * GET /yelps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get business by slug
		$data = Yelp::business( $id );

		return Response::json( $data );
	}

	/**
	 * Display the specified resource.
	 * GET /yelps/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function search()
	{
		$input = Input::all();

		$terms = $input[ 'terms' ];
		$location = $input[ 'location' ];
		$limit = $input[ 'limit' ];

		$data = Yelp::search($terms, $location, $limit);

		return Response::json( $data );

	}

}