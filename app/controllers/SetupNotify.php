<?php

class SetupNotify {


    /**
     * A queue handler that determines what the winning vote is for a given
     * profile ID and decides how to notify the user. Also saves results to the dB.
     *
     * @param $job
     * @param $data
     */
    public function fire($job, $data)
    {

    }

    public function results($job, $data) {

        $queued = DB::table( 'queue' )->where('queue_id', '=', $data[ 'queue_id' ] )->first();

        $profile = Profile::find( $data['profile_id'] );

        if ( true === (bool) $queued->delete || $job->attempts() > 3 ) {
            // Clear the job from the queue table
            $this->cleanUp( $queued->id, $job );
        }

        else {

            $place = $this->initNotify( $data );

            // Only send the notifications if we even have a winning result
            if( false !== $place  && false === (bool) $queued->delete) {
                $url = Config::get('constants.MAP_URL_PREFIX') . $place->coordinates->latitude . "+" . $place->coordinates->longitude;

                $payload = array('place' => $place, 'map_url' => $url);

                Profile::sendEmail($profile->group_email, $payload);

                Profile::sendSlack($profile->webhook_url_in, $place, $url);

                // Save the winner in the db
                DB::table('votes_results')->insert( array( 'yelp_id' => $place->id, 'category' => $place->category, 'profile_id' => $profile->id, 'created_at' => Carbon::now() ) );
            }

            // Flush the profile
            Profile::flushProfile( $profile->id, null, null, true );

            Profile::incrementReminderDate( $profile );
        }

        $this->cleanUp( $queued->id, $job );
    }

    /**
     * Notification handler to suggest places to eat
     *
     * @param $job
     * @param $data
     */
    public function suggestions( $job, $data ) {

        $queued = DB::table( 'queue' )->where( 'queue_id', '=', $data[ 'queue_id' ] )->first();

        $profile = Profile::find( $data[ 'profile_id' ] );


        // Was this job deleted or has it failed too many times?
        if ( $job->attempts() > 1 || true === (bool) $queued->delete )
        {
            $this->cleanUp( $queued->id, $job );

            return;
        }

        // Get suggestions to send via the available notification options
        elseif ( false === (bool) $queued->delete ) {

            // Set the results notification if it wasn't already
            if( false === Profile::checkNotify( $profile->id ) ) {
                Profile::queueResultsMsg( $profile );
            }

            $places = Profile::suggest( $profile->zipcode, Config::get('constants.DEFAULT_NUMBER_SUGGESTIONS'), $profile->id );

            $i = 1;

            // Let's vote on them (all with a vote of "no", so at least
            // they show up on the site but won't be chosen as winners
            foreach( $places as $place ) {

                $place->vote = 0;

                $place->suggest_index = $i;

                Vote::saveVote( $place, $profile->id );

                $i++;
            }

            // Send the suggestion notifications
            $payload = Profile::buildSlackPayload( $places, $profile );

            Profile::sendSlack( $profile->webhook_url_in, null, null, $payload );


            if( true === (bool) $profile->notify_enable ) {

                // Set a notification for tomorrow
                Profile::setDailyReminder( $profile );
            }

            $this->cleanUp( $queued->id, $job );
        }

    }

    /**
     * Notification handler for sending out the very first vote.
     *
     * @param $job
     * @param $data
     */
    public function first( $job, $data ) {

        if ($job->attempts() > 3)
        {
            $job->delete();
        }

        $place = $this->initNotify( $data );

        $app_host = $data[ 'app_host' ];

        $app_url = $app_host . "/" . $data[ 'slug' ];

        $payload = array( 'place' => $place, 'app_url' => $app_url );

        $slack_payload = '{"icon_emoji": ":fork_and_knife:", "username": "' . Config::get( 'constants.APP_NAME') . '", "text": "The Voting Begins!\n\n<' . $place->url .'|'. $place->name . '> (on Yelp)\n\n\n<' . $app_url . '|' . 'Go Vote!> "}';

        Profile::sendEmail( $data[ 'group_email' ], $payload, 'emails.notify-first', 'The voting begins...' );

        Profile::sendSlack( $data[ 'slack_url' ], $place, null, $slack_payload );

        // Remove the job from the queue
        $job->delete();
    }

    public function initNotify( $data ) {

        $result_id = Vote::getWinner( $data[ 'profile_id' ] );

        if( false !== $result_id ) {
            $place = Yelp::business( $result_id );
        }
        else {
            return false;
        }

        return $place[0];
    }

    private function cleanUp( $id, $job ) {
        DB::table( 'queue' )->where( 'id', '=', $id )->delete();

        $job->delete();
    }
}