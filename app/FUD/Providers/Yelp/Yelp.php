<?php namespace FUD\Providers\Yelp;

use Config;

require_once('OAuth.php');

class Yelp
{

    public function __construct()
    {
        $this->key                = Config::get( 'constants.CONSUMER_KEY' );
        $this->secret             = Config::get( 'constants.CONSUMER_SECRET' );
        $this->token              = Config::get( 'constants.TOKEN' );
        $this->token_secret       = Config::get( 'constants.TOKEN_SECRET' );
        $this->api                = Config::get( 'constants.API_HOST' );
        $this->default_term       = Config::get( 'constants.DEFAULT_TERM' );
        $this->default_location   = Config::get( 'constants.DEFAULT_LOCATION' );
        $this->default_limit      = Config::get( 'constants.DEFAULT_LIMIT' );
        $this->search_uri         = Config::get( 'constants.SEARCH_URI' );
        $this->business_uri       = Config::get( 'constants.BUSINESS_URI' );
    }

    /**
     * Makes a request to the Yelp API and returns the response
     *
     * @param $host
     * @param $path
     * @param bool $format
     * @return array|mixed
     */
    public function request($host, $path, $format = true ) {
        $unsigned_url = "http://" . $host . $path;

        // Token object built using the OAuth library
        $token = new \OAuthToken( $this->token, $this->token_secret );

        // Consumer object built using the OAuth library
        $consumer = new \OAuthConsumer( $this->key, $this->secret );

        // Yelp uses HMAC SHA1 encoding
        $signature_method = new \OAuthSignatureMethod_HMAC_SHA1();
        $oauthrequest = \OAuthRequest::from_consumer_and_token(
            $consumer,
            $token,
            'GET',
            $unsigned_url
        );

        // Sign the request
        $oauthrequest->sign_request($signature_method, $consumer, $token);

        // Get the signed URL
        $signed_url = $oauthrequest->to_url();

        // Send Yelp API Call
        $ch = curl_init($signed_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        if($format === true ) {
            $data = $this->formatResponseObject( json_decode( $data ) );
        }

        return $data;

    }

    private function formatResponseObject( $data ) {

        $array = array();

        if( isset( $data->businesses ) && 0 < count( $data->businesses ) ) {

            foreach( $data->businesses as $key => $business ) {

                $array[] = $this->stuffObject( $business );
            }
        }

        // The $data variable is a single business, not an array
        elseif( isset( $data->name ) ) {
            $array[] = $this->stuffObject( $data );
        }
        else {
            // May have service errors (i.e. server time-sync issue
        }

        return $array;
    }

    /**
     * Super hacky data transformer. Need to switch to a proper transformer solution
     * for returning data.
     *
     * @param $data
     * @return object
     */
    private function stuffObject( $data ) {

        // The prototype object to return
        $object =  array (
                    'id'            => null,
                    'url'           => null,
                    'rating'        => null,
                    'name'          => null,
                    'display_phone' => null,
                    'phone'         => null,
                    'image_url'     => null,
                    'cross_streets' => 'location,cross_streets',       // $data->location->cross_streets
                    'address'       => 'location,display_address,0',   //'$data->location->display_address[0]'
                    'address2'      => 'location,display_address,1',   //'$data->location->display_address[0]'
                    'zipcode'       => 'location,postal_code',         // $data->location->postal_code
                    'category'      => 'categories,0,0',
                    'coordinates'   => 'location,coordinate'
                );

        foreach( $object as $key => $value ) {

            $sub_keys = explode( ',' , $value );

            $count = count( $sub_keys );

            // Access the object property according to it's depth
                switch( $count ) {
                    case 2:

                        if( isset( $data->$sub_keys[0]->$sub_keys[1] ) ) {
                            $object[ $key ] = $data->$sub_keys[0]->$sub_keys[1];
                        }
                        elseif( $key === 'coordinates' ) {
                            $object[ $key ] = array( 'lat' => $data->location->coordinate->latitutde, 'lon' => $data->location->coordinate->longitude );
                         }
                        else {
                            $object[ $key ] = null;
                        }
                        break;

                    case 3:

                        if( $key === 'address' && isset( $data->location->display_address[0] ) ) {
                            $object[ $key ] = $data->location->display_address[0];
                        }
                        elseif( $key === 'address2' && isset( $data->location->display_address[2] ) ) {
                            $object[ $key ] = $data->location->display_address[2];
                        }
                        elseif( $key === 'category' && isset( $data->categories[0][0] ) ) {
                            $object[ $key ] = $data->categories[0][0];
                        }
                        else {
                            $object[ $key ] = null;
                        }

                        break;

                    default :
                        if( isset( $data->$key ) ) {
                            $object[$key] = $data->$key;
                        }
                        break;
                }
        }

        return (object) $object;
    }

    /**
     * Query the Search API by a search term and location
     *
     * @param $terms
     * @param $location
     * @param null $limit
     * @param bool $format_output
     * @return The
     */
    public function search($terms, $location, $limit = null, $format_output = true ) {

        $url_params[ 'term' ]       = $terms    ? $terms    : $this->default_term;
        $url_params[ 'location' ]   = $location ? $location : $this->default_location;
        $url_params[ 'limit' ]      = $limit    ? $limit    : $this->default_limit;

        $search = $this->search_uri . "?" . http_build_query($url_params);

        return $this->request( $this->api, $search, $format_output );
    }

    /**
     * Query the Business API by business_id
     *
     * @param $business_id
     * @param bool $format_output
     * @return The
     */
    public function business( $business_id, $format_output = true ) {
        $business = $this->business_uri . $business_id;

        return $this->request( $this->api, $business, $format_output );
    }

    /**
     * Queries the API by the input values from the user
     *
     * @param    $term        The search term to query
     * @param    $location    The location of the business to query
     */
    public function query_api($term, $location) {
        $response = json_decode( $this->search($term, $location));
        $business_id = $response->businesses[0]->id;

        print sprintf(
            "%d businesses found, querying business info for the top result \"%s\"\n\n",
            count($response->businesses),
            $business_id
        );

        $response = $this->business($business_id);

        print sprintf("Result for business \"%s\" found:\n", $business_id);
        print "$response\n";
    }

    public function getSearchLimit() {
        return $this->default_limit;
    }
}