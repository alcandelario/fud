<?php namespace FUD\Providers\Yelp;

use Illuminate\Support\Facades\Facade;

class YelpFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'yelp';
    }
}