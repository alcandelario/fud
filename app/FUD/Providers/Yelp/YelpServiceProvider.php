<?php namespace FUD\Providers\Yelp;

use Illuminate\Support\ServiceProvider;

class YelpServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('yelp', function()
        {
            return new \FUD\Providers\Yelp\Yelp();
        });
    }
}
?>