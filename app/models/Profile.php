<?php

use LaravelBook\Ardent\Ardent;

class Profile extends Ardent {

	protected $table = 'profiles';

	protected $fillable = [
		'user_id',
		'display_name',
		'slug',
		'phone',
		'address',
		'city',
		'state',
		'zipcode',
		'unique_daily_id',
		'group_email',
		'webhook_url_in',
		'webhook_token',
		'reminder_time',
		'utc_offset',
		'is_guest'
	];

	public static $rules = array(
		'user_id'           => 'required'
	);

	public static $relationsData = array(
		'users'				=> array( self::BELONGS_TO, 'User' ),
		'votes'  			=> array( self::BELONGS_TO_MANY, 'Vote' ),
		'blacklists'		=> array( self::HAS_MANY, 'Blacklist' ),
		'favorites'			=> array( self::HAS_MANY, 'Favorite' ),
		'votes_results'		=> array( self::HAS_MANY, 'VoteResult' ),
	);

	public function __toString() {
		return '';
	}

	/**
	 * Return basic profile plus currently selected places along with
	 * other places to choose from if voting pool is too small
	 *
	 * @param $slug
	 * @return object
	 */
	public static function showProfile( $slug, $get_votes, $limit = null ) {

		$profile = (object) DB::table('profiles')->where( 'slug', '=', $slug )->first();

		$profile->is_guest = (bool) $profile->is_guest;

		// Remove old vote data if necessary
		Profile::flushProfile( $profile->id );

		if( str_is( 'true', $get_votes ) ) {

			$voting_pool = Profile::showVotes($profile->id, $profile->zipcode, $limit );

			$profile->voting_pool = $voting_pool;

			$profile = Profile::setWinner( $profile );
		}

		$profile = Profile::localizeTime( $profile );

		$profile = Profile::filterUserData( $profile );

		return $profile;
	}

	private static function filterUserData( $profile ) {

		if( ! Sentry::check() ){
			$profile = Profile::stripData( $profile );
		}
		else {

			$user = Sentry::getUser();

			if( $user->id !== $profile->user_id ) {
				$profile = Profile::stripData( $profile );
			}
		}

		return $profile;
	}

	private static function stripData( $profile ) {

		unset( $profile->group_email );
		unset( $profile->webhook_url_in );
		unset( $profile->webhook_token );
		unset( $profile->notify_enable );

		return $profile;

	}

	private static function setWinner( $profile ) {

		$exp_hours = Config::get('constants.VOTE_EXPIRATION_HOURS');

		$exp_mins = Config::get('constants.VOTE_EXPIRATION_MINUTES');

		$before = Carbon::now()->subHours( $exp_hours )->subMinutes( $exp_mins )->format('Y-m-d H:i:s');

		$winner = DB::table( 'votes_results' )->where( 'profile_id', '=', $profile->id )->where( 'created_at', '>', $before)->orderBy('created_at', 'desc')->first();

		// Make sure we actually need to set a winner before we continue
		if( !empty( $winner ) ) {
			$place = Yelp::business( $winner->yelp_id );

			$profile->winner = $place[0];
		}

		return $profile;
	}

	/**
	 * Get the votes for this profile
	 *
	 * @param $id
	 * @param $zipcode
	 * @return array
	 */
	private static function showVotes( $id, $zipcode = null, $limit = null, $votes_only = false, $vote_limit = null ) {

		$pool = array();

		$votes = Vote::getVotes( $id, $vote_limit );

		// Parse votes for this profile into a voting pool
		foreach( $votes as $vote ) {

			// "Cast" the int to a boolean
			if( $vote->vote === 0) {

				$vote->vote = 'false';
			}

			else {

				$vote->vote = 'true';
			}

			$key = Vote::inVotingPool( $vote->id, $pool );

			// Either add the entire object to the voting pool or add it and
			// comments to the existing pool object
			if( true === is_bool( $key ) ) {

				$vote->votes_comments = array( $vote->vote => array( $vote->comment ) );

				unset($vote->vote);

				unset($vote->comment);

				// Set a flag
				$vote->selected_item = true;

				// Are we trying to limit the number of results in the pool?
				$pool[] = $vote;

			}

			// Add any additional comments into this pool object
			else {
				$pool[$key]->votes_comments[ $vote->vote ][] = $vote->comment;
			}
		}

		$limit = $limit ? $limit : Yelp::getSearchLimit();

		// Add more places to choose from if necessary
		if( $limit > count( $pool ) && false === $votes_only ) {

			$places = Yelp::search( null, $zipcode, $limit );

			foreach( $places as $key => $place ) {

				$in_array = Vote::inVotingPool( $place->id, $pool );

				// Don't add it to the pool if it's already there
				if( is_bool( $in_array ) && false === $in_array ) {

					if( $limit > count( $pool ) ) {

						$place->selected_item = false;

						$pool[] = $place;
					}

					else {
						break;
					}
				}
			}
		}

		return $pool;
	}

	/**
	 * Removes old votes/comments/places from any record that was timestamped BEFORE
	 * the expiration config (set in /config/constants.php
	 *
	 * @param $id
	 */
	public static function flushProfile( $id, $hours = null, $minutes = null, $force = false ) {

		if( false === $force ) {
			$exp_hours = $hours ? $hours : Config::get('constants.VOTE_EXPIRATION_HOURS');

			$exp_mins = $minutes ? $minutes : Config::get('constants.VOTE_EXPIRATION_MINUTES');

			$before = Carbon::now()->subHours($exp_hours)->subMinutes($exp_mins)->format('Y-m-d H:i:s');

			// Remove votes older than the cutoff ( default is T - 12hrs )
			Vote::where('profile_id', '=', $id)->where('created_at', '<', $before)->delete();

			// Lets also flush out old guest profiles while we're at at.
			// This will cascade-delete their votes as well.
			Profile::where('user_id', '=', 1)->where('created_at', '<', $before)->delete();
		}
		else {
			Vote::where('profile_id', '=', $id)->delete();
		}

		// Remove "places" that aren't referenced in the votes_places table
		DB::statement( 'DELETE FROM places WHERE places.id not in (SELECT place_id FROM votes_places)' );

		// Remove "comments" that aren't referenced in the votes_comments table
		DB::statement( 'DELETE FROM comments WHERE comments.id not in (SELECT comment_id FROM votes_comments)' );
	}

	/**
	 * Convert Profile reminder time from UTC to user's local times
	 *
	 * @param $profile
	 * @return mixed
	 */
 	public static function localizeTime( $profile ) {

		$utc_offset = $profile->utc_offset;

		$time = new Carbon( $profile->reminder_time );

		$time = $time->subMinutes( $utc_offset );

		$profile->reminder_time = $time;

		return $profile;
	}

	/**
	 * Create a guest user profile
	 *
	 * @param $input
	 * @return mixed
	 */
	public static function createGuest( $input = null ) {

		$user = DB::table( 'users' )->where( 'email', '=', Config::get( 'constants.DEFAULT_GUEST_EMAIL' ) )->first();

		$slug = $input[ 'slug' ];

		$zipcode = $input[ 'zipcode' ] ? $input[ 'zipcode' ] : Config::get( 'constants.DEFAULT_LOCATION' );

		// Keep trying until we get one that's free
		while( Profile::slugExists( $slug ) ) {
			$slug = str_random( 15 );
		}

		// Create a new guest profile with the unique slug
		$profile = Profile::create( array(

			'user_id' 		=> $user->id,
			'slug' 			=> $slug,
			'display_name'	=> $slug,
			'is_guest' 		=> 1,
			'utc_offset' 	=> $input[ 'utc_offset' ],
			'zipcode'		=> $zipcode,
			'reminder_time' => new Carbon( $input[ 'reminder_time' ] ),
			'notify_enable' => 0,
		));

		$profile = Profile::localizeTime ( $profile );

		return $profile;
	}

	public static function slugExists( $slug ) {

		$profile = DB::table( 'profiles' )->where( 'slug', '=', $slug )->first();

		if( isset( $profile->id ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Check whether or not we need to push a notification job onto the queue
	 *
	 * @param $profile_id
	 * @return bool
	 */
	public static function checkNotify( $profile_id ) {

		$votes = Vote::where( 'profile_id', '=', $profile_id )->get();

		// There are votes already logged for this profile
		if( 0 < count( $votes ) ) {
			return true;
		}

		// There aren't any votes for this profile yet
		return false;
	}

	public static function getReminderTime( $time ) {

		$time = Carbon::createFromTime( $time->hour, $time->minute, $time->second );

		$now = Carbon::now();

		$diff = $now->diffInMinutes( $time, false );

		if( 0 > $diff ) {

			// Time right now is later than reminder time so, add a day
			return $time->addDay();
		}

		return $time;
	}

	/**
	 * Slugify an input string
	 *
	 * @param $text
	 * @return bool|mixed|string
	 */
	public static function slugify( $text )
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text))
		{
			return false;
		}

		return $text;
	}

	/**
	 * Return a list of suggestions based on randomized factors and
	 * on the user's past history
	 *
	 * @param $location
	 * @param null $limit
	 * @param null $id
	 * @return array
	 */
	public static function suggest( $location, $limit = null, $id = false ) {

		$limit = $limit ? $limit : Config::get('constants.DEFAULT_NUMBER_SUGGESTIONS');

		$array_limit = $limit;

		$id = $id ? $id : false;

		$defaults = Config::get( 'constants.DEFAULT_SUGGESTIONS' );

		$return = array();

		$complete = false;

		// Get results based on previous winning result categories
		if( false !== $id ) {

			// Show only votes, not random items thrown into the voting pool
			$return = Profile::showVotes( $id, null, null, true, $limit );

			// Now lets limit the amount of random results we return
			$array_limit = $limit - count( $return );

			if( 0 < $array_limit && count( $return ) < $limit ) {

				$winners = Profile::includeWinners( $id, $array_limit );

				$return = array_merge( $return, $winners );
			}
		}

		$i = 0;

		// Get results based on random categories
		while( !$complete && $array_limit > 0 ) {

			$term = $defaults[ array_rand( $defaults ) ];

			$places = Yelp::search( $term, $location, $limit );  // Return some results

			$place = $places[ array_rand( $places ) ]; // Pick a random one

			if( $i < $array_limit && false === Profile::isDuplicate( $return, $place ) ) {
				$return[] = $place;
				$i++;
			}
			elseif( $i === $array_limit ) {
				$complete = true;
				$array_limit = 0;
			}
		}

		return $return;
	}

	/**
	 * Parse the array to make sure we aren't trying to include something
	 * already in the array
	 *
	 * @param $array
	 * @param $item
	 * @return bool
	 */
	private static function isDuplicate( $array, $item ) {

		foreach( $array as $array_item ) {

			if( str_is( $array_item->id, $item->id ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Create some kind of randomness in the results data that we query,
	 * either by category, date, number of results, etc
	 *
	 * @param $query
	 * @return mixed
	 */
	private static function modifySuggestQuery( $query ) {

		return $query;
	}

	/**
	 * Let's include previous winning items but only if we have a
	 * decent history of results already
	 *
	 * ********** Could extend this by including new results based on meta data from winners **********
	 *
	 * @param $id
	 * @param $results_limit
	 * @param int $include_limit
	 * @return array
	 */
	public static function includeWinners( $id, $results_limit, $include_limit = 1 ) {

		$results = DB::table( 'votes_results' )->where( 'profile_id', '=', $id )->take( $results_limit );

		$results = Profile::modifySuggestQuery( $results )->get();

		$return = array();

		if( $results_limit < count( $results ) ) {

			$complete = false;

			$i = 0;

			$retry = 5;

			// Try for only so long to include winning results
			while( !$complete ) {
				$place = $results[ array_rand( $results ) ];

				$place = Yelp::business( $place->yelp_id );

				$place = $place[0];

				if( $i < $include_limit && !Profile::isDuplicate( $return, $place ) ) {
					$return[] = $place;
					$i++;
				}

				// We've got enough, let's exit
				elseif( $i == $include_limit ) {
					$complete = true;
				}

				// Decrement the retry counter
				else {
					$retry--;
				}

				if( 0 === $retry ){
					$complete = true;
				}
			}

			return $return;
		}

		return $return;
	}

	public static function setInitialReminders( $profile, $skip_first = false ) {

		// Send notification(s) for the very first vote, but not if slack
		// voting is set up. That should be used as the initial reminder
		if( ( 1 > strlen( $profile->webhook_token ) )  && ( false === $skip_first ) ) {
			Profile::queueFirstMsg( $profile );
		}

		Profile::queueResultsMsg( $profile );

	}

	/**
	 * Set profile reminder to send via available notification options
	 * Currently only supports slack
	 *
	 * @param $profile
	 */
	public static function setDailyReminder( $profile ) {

		if( isset( $profile->webhook_token ) ) {

			Profile::incrementReminderDate( $profile );

			$default= new Carbon( Config::get( 'constants.DEFAULT_REMINDER_TIME' ) );

			// Get the profile's reminder time or create a default time from the user's utc_offset
			$reminder_time = isset( $profile->reminder_time ) ? $profile->reminder_time : $default->addMinutes( $profile->utc_offset );

			$reminder = new Carbon( $reminder_time );


			// Create a reminder 2 hours before tomorrow's reminder time
			// if not in test mode
			if( true === Config::get( 'constants.SLACK_TEST_MODE' ) ) {
				$time = Carbon::now()->addMinutes( 2 );
			}
			else {

				$now = Carbon::now();

				$t = $reminder->subHours( 2 );

				$diff = $now->diffInMinutes( $t, false );

				$time = $now->addMinutes( $diff );

				if( $diff <= 0 ) {
					$time = $time->addDay();
				}
			}

			// Generate a new id for this notification
			$queue_id = 'sug_' . Profile::randomID();

			Profile::queueSuggestionMsg( $time, $profile, $queue_id );

			// Set this notification in the queue
			Profile::queueLog( $profile->id, $queue_id, $time );
		}

	}

	private static function nowAndReminderDiff( $profile ) {

		$default= new Carbon( Config::get( 'constants.DEFAULT_REMINDER_TIME' ) );

		// Get the profile's reminder time or create a default time from the user's utc_offset
		$reminder_time = isset( $profile->reminder_time ) ? $profile->reminder_time : $default->addMinutes( $profile->utc_offset );

		$reminder = new Carbon( $reminder_time );


		// Create a "now()" Carbon object using the reminder date params, the only difference will be the time
		$now = Carbon::createFromDate( $reminder->year, $reminder->month, $reminder->day );

		return $now->diffInMinutes( $reminder, false );
	}

	/**
	 * Increment the reminder date in the dB if it is older than the
	 * current time
	 *
	 * @param $profile
	 */
	public static function incrementReminderDate( $profile ) {

		$now = Carbon::now();

		// Increment the reminder date, not the time though
		$t = new Carbon( $profile->reminder_time );

		if( 0 > $now->diffInMinutes( $t ) ) {

			$profile->reminder_time = $t->addDay();

			$profile->save();
		}
	}

	public static function queueFirstMsg( $profile ) {

		if( false === (bool) $profile->is_guest ) {
			// Set notification alerting users of very first vote
			Queue::push('SetupNotify@first',
				array(
					'profile_id' => $profile->id,
					'slug' => $profile->slug,
					'group_email' => $profile->group_email,
					'slack_url' => $profile->webhook_url_in,
					'app_host' => Session::get('app_host'),
				)
			);
		}
	}

	public static function queueResultsMsg( $profile ) {

		$diff = Profile::nowAndReminderDiff( $profile );


		// Should we set the "results" notification?
		// Only if the reminder time is in the future
		if( 0 < $diff ) {

			// Generate a unique identifier for this notification
			$queue_id = 'res_' . Profile::randomID();

			$time = Carbon::now()->addMinutes( $diff );

			// Set the notification where vote results are determined
			Queue::later( $time, 'SetupNotify@results',
				array(
					'profile_id' 		=> $profile->id,
					'group_email'	 	=> $profile->group_email,
					'slack_url'			=> $profile->webhook_url_in,
					'queue_id'			=> $queue_id,
				)
			);

			// Set this notification in the queue
			Profile::queueLog( $profile->id, $queue_id, $time );
		}
	}

	public static function queueSuggestionMsg( $time, $profile, $queue_id ) {
		Queue::later( $time, 'SetupNotify@suggestions',
			array(
				'profile_id' 		=> $profile->id,
				'group_email'	 	=> $profile->group_email,
				'slack_url'			=> $profile->webhook_url_in,
				'queue_id'			=> $queue_id,
			)
		);
	}

	public static function randomID() {
		return str_random( 10 );
	}

	public static function queueLog( $profile_id, $queue_id, $time ) {

		// Log this job on the queue in case we need to change how this is processed later
		DB::table( 'queue' )->insert( array( 'profile_id' => $profile_id, 'queue_id' => $queue_id, 'delete' => 0, 'created_at' => Carbon::now(), 'updated_at' => $time ) );
	}

	public static function deleteQueuedJobs( $id ) {

		$queue = DB::table( 'queue' )->select( 'id' )->where( 'profile_id', '=', $id )->where( 'delete', '=', 0 )->get();

		// Set all previous queue jobs for this Profile to delete themselves upon execution
		foreach( $queue as $index => $item ) {
			DB::table( 'queue' )->where( 'id', '=', $item->id )->update( array( 'delete' => 1 ) );
		}
	}

	public static function buildSlackPayload( $places, $profile ) {

		$middle = '';

		$begin = '{"icon_emoji": ":fork_and_knife:", "username": "' . Config::get('constants.APP_NAME') . '", "text": "To make a choice, type \"eat:\" followed by a number (e.g. eat:3 ) \n\nTo choose more than one, separate with commas (e.g. eat:2,4,1)\n\nOr, use the app and <' . Request::root() . $profile->slug . '|find something else>\n\n';

		$end = '"}';

		foreach( $places as $place ) {

			$place_link = '<' . $place->url . '|' . $place->name . '>';

			$middle = $middle . $place->suggest_index . ". " . $place_link . ' - ' . $place->address . '\n\n';
		}

		$payload = $begin . $middle . $end;

		return $payload;
	}

	public static function sendSlack( $webhook, $place = null, $map_url = null, $payload = null ) {

		if( 0 < strlen( $webhook ) ) {

			$client = new \GuzzleHttp\Client();

			if( isset( $place ) ) {
				$default_payload = '{"icon_emoji": ":fork_and_knife:", "username": "' . Config::get('constants.APP_NAME') . '", "text": "Time to eat.\n\n<' . $place->url . '|' . $place->name . '> (on Yelp)\n\n\n<' . $map_url . '|' . $place->address . '> (Google Map)"}';
			}

			$payload = $payload ? $payload : $default_payload;


			$client->post( $webhook, array('body' => array( "payload" => $payload ) ) );
		}
	}

	public static function sendEmail( $notify_email, $payload, $template = null, $subject = null ) {

		if( 0 < strlen( $notify_email ) ) {
			$template = $template ? $template : 'emails.notify-results';

			$params[ 'name' ]       = Config::get( 'constants.EMAIL_NAME' );
			$params[ 'subject' ]    = $subject ? $subject : Config::get( 'constants.EMAIL_RESULTS' );
			$params[ 'from' ]       = Config::get( 'constants.EMAIL_FROM' );

			// Fire off the winner email
			Mail::send( $template, $payload, function( $message ) use ( $notify_email, $params )
			{

				$message->subject( $params['subject'] );

				$message->from( $params[ 'from' ], $params[ 'name' ] );

				$message->to( $notify_email );

			});
		}
	}
}