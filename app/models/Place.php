<?php

use LaravelBook\Ardent\Ardent;

class Place extends Ardent {

	protected $table = 'places';

	protected $fillable = ['yelp_id', 'url', 'rating','name','display_phone', 'phone','image_url','cross_streets','address','zipcode'];

	public static $rules = array(
		'yelp_id'			=> 'required'
	);
}