<?php

use LaravelBook\Ardent\Ardent;

class Vote extends Ardent {

	protected $table = 'votes';

	protected $fillable = [ 'profile_id', 'vote', 'suggest_index' ];

	public static $rules = array(
		'profile_id'			=> 'required',
		'vote'					=> 'required'
	);

	public static $relationsData = array(
		'profiles'  		=> array(self::HAS_ONE, "Profile" )
	);

	public function __toString() {
		return '';
	}

	public static function getVotes( $id, $limit = null ) {

		$data = DB::table( 'votes' )
					->leftJoin( 'votes_comments', 'votes.id', '=', 'votes_comments.vote_id' )
					->join( 'votes_places', 'votes_places.vote_id', '=', 'votes.id' )
					->leftJoin( 'comments', 'votes_comments.comment_id', '=', 'comments.id')
					->join( 'places', 'votes_places.place_id', '=', 'places.id' )
					->select( 'places.yelp_id AS id', 'votes.vote', 'comments.comment', 'places.url', 'places.rating', 'places.name', 'places.display_phone', 'places.phone', 'places.image_url', 'places.cross_streets', 'places.address', 'places.zipcode' )
					->where( 'votes.profile_id', '=', $id );

		if( isset( $limit ) ) {
			$data->take( $limit );
		}

		return $data->get();
	}

	public static function saveVote( $item, $id ) {

		$item = (array) $item;

		if( !isset( $item[ 'suggest_index' ] ) ) {
			$index = 0;
		}
		else {
			$index = $item[ 'suggest_index' ];
		}

		$vote = Vote::create( array( 'profile_id' => $id, 'vote' => $item[ 'vote' ], 'suggest_index' => $index ) )->attributes[ 'id' ];

		// Save comments if any
		if( isset( $item[ 'new_comment' ] ) && 0 < strlen( $item[ 'new_comment' ] ) ) {

			$comment = DB::table( 'comments' )->insertGetId( array( 'comment' => $item[ 'new_comment' ] ) );

			DB::table( 'votes_comments' )->insertGetId( array( 'vote_id' => $vote, 'comment_id' => $comment) );
		}

		// Save vote location
		$place = Place::create( array(
			'yelp_id' 		=> $item[ 'id' ],
			'name' 			=> $item[ 'name' ],
			'address' 		=> $item[ 'address' ],
			'cross_streets' => $item[ 'cross_streets' ],
			'display_phone' => $item[ 'display_phone' ],
			'phone' 		=> $item[ 'phone' ],
			'image_url' 	=> $item[ 'image_url' ],
			'rating' 		=> $item[ 'rating' ],
			'url' 			=> $item[ 'url' ],
			'zipcode' 		=> $item[ 'zipcode' ]
		));

		// Populate the pivot table
		DB::table( 'votes_places' )->insertGetId( array(
			'vote_id' 	=> $vote,
			'place_id'	=> $place->attributes[ 'id' ]
		));

		return array( 'saved' => true );
	}

	/**
	 * Parse the vote results for a default zipcode for a guest user
	 *
	 * @param $votes
	 * @return mixed
	 */
	public static function getDefaultZip( $votes ) {

		foreach( $votes as $id => $place ) {

			if ( $place[ 'vote' ] === true ) {
				return $place[ 'zipcode' ];
			}
		}

		return $place[ 'zipcode' ];
	}

	/**
	 * Confirm the item isn't already int the voting pool
	 *
	 * @param $id
	 * @param $pool
	 * @return bool
	 */
	public static function inVotingPool( $id, $pool ) {

		if( 0 < count( $pool ) ) {

			foreach ($pool as $key => $vote) {

				if ($id === $vote->id) {

					return $key;
				}
			}

			return false;
		}
		else {
			return false;
		}
	}

	public static function getWinner( $id ) {

		// Find the winning vote
		$results = DB::table( 'votes' )
			->join( 'votes_places', 'votes_places.vote_id', '=', 'votes.id' )
			->join( 'places', 'votes_places.place_id', '=', 'places.id')
			->select('places.yelp_id as id')
			->where( 'votes.profile_id', '=', $id )
			->where( 'votes.vote', '=', 1 )
			->get();

		// Return the yelp id with the most "yes" votes
		return Vote::parseResults( $results );
	}

	private static function parseResults ( $results ) {

		$array = array();

		// Parse results into buckets
		foreach( $results as $result ) {

			if( !array_key_exists( $result->id, $array ) ) {

				// Init a key, value in the array where the key is the yelp_id
				$array[ $result->id ] = 1;
			}
			else {

				// Increment the vote count for this yelp id
				$num = $array[ $result->id ];

				$array[ $result->id ] = $num + 1;
			}
		}

		$count = 0;

		$id = false;

		// Find the one with the most votes
		foreach( $array as $place_id => $num_votes ){

			if( $num_votes >= $count ) {
				$count = $num_votes;
				$id = $place_id;
			}
		}

		return $id;
	}
}