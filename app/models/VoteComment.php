<?php

use LaravelBook\Ardent\Ardent;

class VoteComment extends Ardent {

    protected $table = 'votes_comments';

    protected $fillable = [ 'vote_id', 'comment' ];

    public static $rules = array(
        'vote_id'			=> 'required',
        'comment'			=> 'required'
    );

    public static $relationsData = array(
        'votes'  			=> array(self::HAS_ONE, 'Vote' ),
    );


    public function __toString() {
        return '';
    }

}