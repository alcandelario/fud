<?php

$utc_offset = ( Carbon::now()->offset )/60;                                     // UTC offset in minutes

$array = array(

    'APP_NAME'              => "Nomz",

    /*      Yelp API configuration          */
    'CONSUMER_KEY'                  => "nUZMSFH9xXvfDvqA8jThGQ",
    'CONSUMER_SECRET'               => "8NVpF1oSN-UBRcF0nNiKNgkK428",
    'TOKEN'                         => "sv16ORaiQfHbHLzvzw4I9xaU0-bhAiWX",
    'TOKEN_SECRET'                  => "1Ua3P8Y80dx7zDfGjbeoNYIr1gs",
    'API_HOST'                      => 'api.yelp.com',
    "DEFAULT_TERM"                  => 'lunch',
    "DEFAULT_LOCATION"              => '60657',
    "DEFAULT_LIMIT"                 => 10,
    "DEFAULT_TIMEZONE"              => 'Atlantic/Reykjavik',
    "SEARCH_URI"                    => '/v2/search/',
    "BUSINESS_URI"                  => '/v2/business/',
    /*      END YELP API CONFIG     */

    /*      SUggestions    */
    'DEFAULT_NUMBER_SUGGESTIONS'    => 5,
    'DEFAULT_SUGGESTIONS'           => array( 'bagel', 'sushi', 'eggs', 'burger', 'salad', 'breakfast', 'asian', 'mexican', 'pizza', 'sandwich', 'chicken', 'hot dog', 'beef', 'tacos', 'indian', 'vegetarian' ),


    /*      Expiration time for votes       */
    'VOTE_EXPIRATION_HOURS'         => 2,
    'VOTE_EXPIRATION_MINUTES'       => 30,
    'DEFAULT_REMINDER_TIME'         => null,        // 11:45am UTC time
    'DEFAULT_UTC_OFFSET'            => 360,         // Number of minutes offset from UTC time (e.g. 360mins for Chicago in winter)
    'DEFAULT_GUEST_EMAIL'           => 'guest@guest.com',


    /*      Email Settings                  */
    'EMAIL_NAME'                    => 'Nomz',
    'EMAIL_FROM'                    => 'thenomzapp@gmail.com',
    'EMAIL_RESET'                   => 'Password Reset',
    'EMAIL_RESULTS'                 => 'The results are in!',

    /*      Google Map URL prefix (for emails)
     * z = zoom level
     * t = map type ( m = map, k = satellite, h = hybrid, p = terrain, e = Google Earth )
     * q=loc:  expects a latitude and longitude, separated by a + sign
     */

    'MAP_URL_PREFIX'                => "http://maps.google.com/maps?z=12&t=m&q=loc:",


    /*      DEBUG      */
    'SLACK_TEST_MODE'               => true,
);

$time = Carbon::createFromTime( 11, 45, 00, $array[ 'DEFAULT_TIMEZONE' ] ); 	// 15 mins before noon, UTC 00:00

$reminder_time = $time->addMinutes( $utc_offset );

$array[ 'DEFAULT_REMINDER_TIME' ] = $time;

return $array;