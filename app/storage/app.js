/**
 * Created by alcandelario on 12/3/14.
 */
var app = angular.module( "fud", [
    'ngResource',
    'ui.bootstrap',
    'ui.router',
    'ngSanitize',
    'ngCookies',
    'http-auth-interceptor',
    'slugifier',
    'toaster'
] );

app.config( function( $stateProvider, $urlRouterProvider, $locationProvider) {

    // Remove Hash symbol from URLs
    $locationProvider.html5Mode(true);

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state( 'home' , {
            url:            "/",
            templateUrl:    "/views/home.html"
        })
        .state( 'profile', {
            url:            "/:profile_slug",
            templateUrl:    "/views/profile-main.html"
        })

        .state( 'profile-edit', {
            url:            "/profile/edit/:profile_slug",
            templateUrl:     "/views/profile-edit.html"
        })

        .state( 'profile-register', {
            url:            "/profile/register",
            templateUrl:    "/views/profile-register.html"
        })

        .state( 'login', {
            url:            "/profile/login",
            templateUrl:    "/views/login.html"
        })

        .state( 'password-forget', {
            url:            "/profile/password-reset",
            templateUrl:    "/views/forgot-password.html"
        })

        .state( 'password-reset', {
            url:            "/password-reset/id/:id/reset/:reset_code",
            templateUrl:    "/views/reset-password.html"
        })
});

app.run([
    '$http',
    '$location',
    '$state',
    '$stateParams',
    '$rootScope',
    'CSRF_TOKEN',
    'Yelp',
    'Votes',
    'User',
    'Profile',
    'ENV_VARS',
    'BodyClass',
    '$cookieStore',

    function( $http, $location, $state, $stateParams, $rootScope, CSRF_TOKEN, Yelp, Votes, User, Profile, ENV_VARS, BodyClass, $cookieStore ){

        $http.defaults.headers.common['csrf_token'] = CSRF_TOKEN;

        // Initialize app objects
        $rootScope.yelp         = Yelp();
        $rootScope.votes        = Votes();
        $rootScope.user         = User();
        $rootScope.bodyClass    = BodyClass();
        $rootScope.page_bg      = '';
        $rootScope.profile      = Profile();

        // Because we're using http 401 interceptor, set a watch here
        $rootScope.$on('event:auth-loginRequired', function() {
            $rootScope.user.logged_in = false;
        });

        $rootScope.user.isAuthenticated().then( function ( response ) {

            // Initialize the profile object for the "profile" page
            angular.copy( $rootScope.user.profile, $rootScope.profile );
        });

        $rootScope.BASE_URL = ENV_VARS.BASE_URL = $location.host();
    }
]);

app.constant( "ENV_VARS", {
    'EMAIL_ACTIVATION_ENABLED'      : false,
    'LOGIN_PARTIAL'                 : false,        // Is the login form a partial inserted by a directive or is it a URL route?
    'BASE_URL'                      : null,
    'SEARCH_RETURN_LIMIT'           : 15,
    'SEARCH_DISPLAY_LIMIT'          : 9,
    'VOTE_LIMIT'                    : 3
});