<!doctype html>
<html lang="en" data-ng-app="fud">
<head>
	<base href="/" />
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="shortcut icon" href="/icons/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" href="/icons/apple-touch-icon.png" />
	<link rel="apple-touch-icon-precomposed" sizes="57x57"   href="/icons/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72"   href="/icons/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" 	 href="/icons/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/icons/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/icons/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/icons/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/icons/apple-touch-icon-152x152.png" />

	<title><?php echo Config::get( 'constants.APP_NAME'); ?> - Time to eat</title>

	<!-- Latest compiled and minified CSS -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>

	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

	<link href='http://fonts.googleapis.com/css?family=Karma:400,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="css/vendor/flat-ui/dist/css/flat-ui.min.css" >

	<link href="//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/0.4.4/toaster.css" rel="stylesheet" />

	<link rel="stylesheet" href="css/app.css">

</head>
<body class="{{bodyClass.class}}">
	<div class="bg-{{bodyClass.background}}"></div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div data-ng-include="'/views/site-header.html'"></div>

			<section class="app-main-container" ui-view >

			</section>

			<toaster-container></toaster-container>

		</main><!-- #main -->
	</div><!-- #primary -->

	<!-- Angular -->
	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router.min.js"></script>

	<script src='//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.3/angular-resource.min.js'></script>

	<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular-sanitize.js'></script>

	<script src='//ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular-cookies.js'></script>

	<script src='//cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.12.0/ui-bootstrap-tpls.min.js'></script>

	<script src="//code.angularjs.org/1.3.5/angular-animate.min.js" ></script>

	<script src="//cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/0.4.4/toaster.js"></script>

	<script src='app/app.js'></script>

	<script src='app/controllers.js'></script>

	<script src='app/directives.js'></script>

	<script src='app/services.js'></script>

	<script src='app/vendor/http-auth-interceptor.js'></script>

	<script src='app/vendor/angular-slugify.js'></script>

	<script src='app/vendor/toastr.js'></script>



	<script>
		angular.module("fud").constant("CSRF_TOKEN", '<?php echo csrf_token(); ?>');
	</script>

</body>
</html>
