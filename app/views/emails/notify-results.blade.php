

<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body style="min-height: 300px;">

		<table style="border-collapse:separate!important;border-spacing:0;width:100%;background:#f6f6f6" width="100%">
			<tbody>
				<tr>
        			<td style="vertical-align:top" valign="top"></td>
        			<td style="vertical-align:top;display:block;max-width:580px;width:580px;margin:0 auto;padding:10px" valign="top">
        				<div style="display:block;max-width:580px;margin:0 auto;padding:10px">
        					<table style="border-collapse:separate!important;border-radius:3px;border-spacing:0;width:100%;background:#fff;border:1px solid #e9e9e9" width="100%">
        						<tbody>
        							<tr>
        								<td style="vertical-align:top;padding:30px" valign="top">
        									<table style="border-collapse:separate!important" width="100%">
        										<tbody>
        											<tr>
        												<td style="vertical-align:top" valign="top">
        													{{--<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:22px;font-weight:normal;margin:0 0 30px 0;padding:0">Yay! Time to eat.</p>--}}
        													{{--<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:30px;font-weight:normal;margin:0 0 30px 0;padding:0">Lunchtime!</p>--}}
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:30px;font-weight:normal;margin:0 0 30px 0;padding:0"><a href="{{$place->url}}" style='text-decoration:none; color:#e74c3c;' target="_blank">{{ $place->name }}</a><span style="font-size: 16px; color:#000">&nbsp;&nbsp;(See it on Yelp)</span></p>
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:18px;font-weight:normal;margin:0 0 7px 0;padding:0">{{ $place->address }}</p>
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:18px;font-weight:normal;margin:0 0 30px 0;padding:0">{{ $place->address2 }}</p>
        														<table style="border-collapse:separate!important;margin-bottom:40px;width:auto" width="auto">
        															<tbody>
        																<tr>
        																	<td style="vertical-align:top;border-radius:5px;text-align:center;background:#348eda" align="center" valign="top">


                      															<a href="{{ $map_url }}" style="border-radius:5px;color:#fff;text-decoration:none;display:inline-block;font-size:14px;font-weight:bold;text-transform:capitalize;background:#1abc9c;margin:0;padding:12px 25px;border:none" target="_blank">See it on Google Maps</a>
                        													</td>
                      													</tr>
																	</tbody>
																</table>
        													{{--<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:14px;font-weight:normal;margin:0 0 15px;padding:0">We may need to communicate important service level issues with you from time to time, so it's <strong>important we have an up-to-date email address</strong> for you on file.</p>--}}
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:14px;font-weight:normal;margin:0 0 15px;padding:0">Happy Eating!</p>
        												</td>
        											</tr>
												</tbody>
											</table>
        								</td>
        							</tr>
								</tbody>
							</table>
        				</div>
        			</td>
        			<td style="vertical-align:top" valign="top"></td>
        		</tr>
			</tbody>
		</table>
	</body>
</html>


