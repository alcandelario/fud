<?php $url = '//fud.app:8000/password-reset/'.$reset; ?>

<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body style="height: 300px;">
		{{--<h2>Password Reset</h2>--}}

		{{--<div>--}}
			{{--We were told you may need to reset your FUD password.--}}
		{{--</div>--}}


        {{--<div style="margin-top: 50px;">--}}
			{{--<a href="{{ URL::to( $url ) }}"--}}
			{{--style="--}}
			{{--padding: 15px 30px;--}}
			{{--background-color: rgb(211,51,51);--}}
			{{--color: #fff;--}}
			{{--text-decoration: none;--}}
			{{--border-radius: 4px;--}}
			{{--">--}}
			{{--Reset Password--}}
			{{--</a>--}}

		    {{--<br/>--}}

		{{--</div>--}}

		<table style="border-collapse:separate!important;border-spacing:0;width:100%;background:#f6f6f6" width="100%">
			<tbody>
				<tr>
        			<td style="vertical-align:top" valign="top"></td>
        			<td style="vertical-align:top;display:block;max-width:580px;width:580px;margin:0 auto;padding:10px" valign="top">
        				<div style="display:block;max-width:580px;margin:0 auto;padding:10px">
        					<table style="border-collapse:separate!important;border-radius:3px;border-spacing:0;width:100%;background:#fff;border:1px solid #e9e9e9" width="100%">
        						<tbody>
        							<tr>
        								<td style="vertical-align:top;padding:30px" valign="top">
        									<table style="border-collapse:separate!important" width="100%">
        										<tbody>
        											<tr>
        												<td style="vertical-align:top" valign="top">
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:14px;font-weight:normal;margin:0 0 15px;padding:0">Thanks for signing up! Please activate your account by clicking the button below.</p>
        														<table style="border-collapse:separate!important;margin-bottom:15px;width:auto" width="auto">
        															<tbody>
        																<tr>
        																	<td style="vertical-align:top;border-radius:5px;text-align:center;background:#348eda" align="center" valign="top">
                      															<a href="SOME ACTIVATION URL WITH TOKEN HERE" style="border-radius:5px;color:#fff;text-decoration:none;display:inline-block;font-size:14px;font-weight:bold;text-transform:capitalize;background:#348eda;margin:0;padding:12px 25px;border:1px solid #348eda" target="_blank">Activate FUD account</a>
                        													</td>
                      													</tr>
																	</tbody>
																</table>
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:14px;font-weight:normal;margin:0 0 15px;padding:0">We may need to communicate with you from time to time, so it's <strong>important we have an up-to-date email address</strong> for you on file.</p>
        													<p style="font-family:'Helvetica Neue',Helvetica,Arial,'Lucida Grande',sans-serif;font-size:14px;font-weight:normal;margin:0 0 15px;padding:0">Thanks and happy eating!</p>
        												</td>
        											</tr>
												</tbody>
											</table>
        								</td>
        							</tr>
								</tbody>
							</table>
        				</div>
        			</td>
        			<td style="vertical-align:top" valign="top"></td>
        		</tr>
			</tbody>
		</table>
	</body>
</html>


