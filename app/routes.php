<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Serve up the angular app
Route::get('/', function()
{
	return View::make('index');
});

Route::group([ 'prefix' => 'api' ], function() {

	// Submit votes
	Route::post('vote',											array('uses' => 'VotesController@store'));
	Route::post('vote/slack',									array('uses' => 'VotesController@storeSlack'));

	// Yelp related
	Route::group([ 'prefix' => 'yelp' ], function () {

		// Yelp business API route
		Route::get('business/{yelp_id}',     					array('uses' => 'YelpController@show'));

		// Yelp search API route
		Route::post('search',									array('uses' => 'YelpController@search'));
	});

	// User related
	Route::group( [ 'prefix' => 'user' ], function() {
		Route::post( 'login',	 								array( 'uses' => 'UsersController@login' )	);
		Route::get( 'logout', 									array( 'uses' => 'UsersController@logout' )	);
		Route::get( 'is-authenticated', 						array( 'uses' => 'UsersController@isAuthenticated' ) );
		Route::post( 'password-forget', 						array( 'uses' => 'UsersController@passwordForget' ) );
		Route::post( 'password-reset', 							array( 'uses' => 'UsersController@passwordReset' ) );
	});

	// Profile related
	Route::group( [ 'prefix' => 'profile' ], function() {
		Route::post( 'register', 								array( 'uses' => 'ProfilesController@register' ) );
		Route::post( 'create-guest', 							array( 'uses' => 'ProfilesController@createGuest' ) );
		Route::post( 'show', 									array( 'uses' => 'ProfilesController@show' ) );
		Route::get( 'exists/{slug}', 							array( 'uses' => 'ProfilesController@slugExists' ) );
	});

	// Authentication-first routes
	Route::group( array( 'before' => 'serviceAuth' ), function () {
		Route::post( 'profile/update', 							array('uses' => 'ProfilesController@update' ) );
		Route::get( 'profile/update-notification/{profile}', 	array('uses' => 'ProfilesController@updateNotify' ) );
	});
});



// =============================================
// CATCH ALL ROUTE =============================
// =============================================
// All routes that are not home or api will be redirected to the frontend
// this allows angular to route them
App::missing( function( $exception )
{
	return View::make( 'index' );
});